class Character < ApplicationRecord
  validates :nome,
    presence: true
    uniqueness: true
  validates :str, :agi, :res,
    presence: true
    numericality: true
    if: :EstaDentroDoLimite?

  def EstaDentroDoLimite?
    str + agi + res <= 10
  end
end
